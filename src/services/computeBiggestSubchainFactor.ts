/* eslint-disable prettier/prettier */
export default function computeBiggestSubchainfactor(
  chain: string,
  n: number
): number {
  if (chain.length < n)
    return 0;
  if (isNaN(parseInt(chain)))
    return 0;
  
  const numbers = chain.split('').map(function(item) {
    return parseInt(item);
  });
  const subList = numbers.slice(0, n);

  let res = multiply(subList);

  for(let i = n; i < chain.length; i++) {
    subList.shift();
    subList.push(numbers[i]);
    const temp = multiply(subList);
    res = temp > res ? temp : res;
  }

  return res;
}

function multiply(array: number[]): number {
  return array.reduce((a, b) => a * b);
}
